@extends('layouts.main');
@section('content')
    <div class="col-md-30">
        <div class="card">
          <div class="card-header">
            <div class="horiz">
                <h3>Liste des pays</h3>
                <a href="{{ route('lands.create') }}" class="btn btnAction">
                  <button type="button" class="btn btn-block bg-gradient-primary btn-sm">
                    Ajouter un pays
                  </button>
                </a>
            </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Code indicatif</th>
                  <th>Libellé</th>
                  <th>Continent</th>
                  <th>Population</th>
                  <th>Capital</th>
                  <th>Superficie</th>
                  <th>Langue</th>
                  <th>Monnaie</th>
                  <th>Laïque</th>
                  <th>Description</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($lands as $land)
                  @if ($land->est_laique === 1)
                    <tr>
                        <td>{{$land->id}}</td>
                        <td>{{$land->code_indicatif}}</td>
                        <td>{{$land->Libelle}}</td>
                        <td>{{$land->Continent}}</td>
                        <td>{{$land->population}}</td>
                        <td>{{$land->capital}}</td>
                        <td>{{$land->Superficie}}</td>
                        <td>{{$land->langue}}</td>
                        <td>{{$land->monnaie}}</td>
                        <td><span class="badge bg-primary">oui</span></td>
                        <td>{{$land->description}}</td>
                        <td id="action">
                          <a href="{{ route('lands.edit', ['id'=> $land->id]) }}">
                            <button class="btn btn-primary dim btnAction" type="button">
                              modifier
                            </button>
                          </a>

                          <form id = "form-{{$land->id}}" action="{{ route('lands.destroy', ['id'=> $land->id]) }}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="delete" />
                            <a href="#" {{-- onclick="if(confirm('voulez vous vraiment supprimer ce pays ?')){document.getElementId('form-{{$land->id}}').submit()}" --}}>
                              <button class="btn btn-danger  dim btnAction" type="submit">
                                supprimer
                              </button>
                            </a>
                          </form>
                        </td>
                    </tr>
                  @else
                    <tr>
                        <td>{{$land->id}}</td>
                        <td>{{$land->code_indicatif}}</td>
                        <td>{{$land->Libelle}}</td>
                        <td>{{$land->Continent}}</td>
                        <td>{{$land->population}}</td>
                        <td>{{$land->capital}}</td>
                        <td>{{$land->Superficie}}</td>
                        <td>{{$land->langue}}</td>
                        <td>{{$land->monnaie}}</td>
                        <td><span class="badge bg-warning">non</span></td>
                        <td>{{$land->description}}</td>
                        <td id="action">
                          <a href="{{ route('lands.edit', ['id' => $land->id]) }}">
                              <button class="btn btn-primary dim btnAction" type="button">
                                modifier
                              </button>
                          </a>

                          <form id = "form-{{$land->id}}" action="{{ route('lands.destroy', ['id'=> $land->id]) }}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="delete" />
                            <a href="#" {{-- onclick="if(confirm('voulez vous vraiment supprimer ce pays ?')){document.getElementId('form-{{$land->id}}').submit()}" --}}>
                              <button class="btn btn-danger  dim btnAction" type="submit">
                                supprimer
                              </button>
                            </a>
                          </form>
                        </td>
                    </tr>
                  @endif
                @endforeach
                
              </tbody>
            </table>
          </div>
          <style>
            /*tr,td,th 
            {
              border: 1px solid black;

            }*/
            #action
            {
              display:flex;
              flex-direction: row;
            }
            h3
            {
                font-size: 2em;
            }
            .horiz
            {
                display: flex;
                flex-direction: row;
            }
            .btnAction
            {
              margin-left: 1em;
            }
          </style>
          <!-- /.card-body -->
    </div>
@endsection