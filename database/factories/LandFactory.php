<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Land>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $continent = array("Afrique", "Europe", "Asie", "Amerique", "Océanie", "Antartique");
        $cleContinent = array_rand($continent,1);

        $monnaie = array("XOF", "EUR", "DOLLAR");
        $cleMonnaie = array_rand($monnaie,1);

        $langue = array("FR", "EN", "AR", "ES");
        $cleLangue = array_rand($langue,1);
        return [
            'Libelle' => $this->faker->country(),
            'description' => $this->faker->paragraph(),
            //'code_indicatif' => $this->faker->cityPrefix(),
            'code_indicatif' => $this->faker->numerify('+###'),
            // 'Continent' => $this->faker->continent(),
            'Continent' => $continent[$cleContinent],
            'population' => rand(400000,100000000),
            'capital' => $this->faker->city(),
            'monnaie' => $monnaie[$cleMonnaie],
            'langue' => $langue[$cleLangue],
            'Superficie' => rand(50000,20000000),
            'est_laique' => $this->faker->boolean(),

        ];
    }
}
