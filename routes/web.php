<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LandController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/                                       		                                      

Route::get('/', function () {
    return view('layouts.main');
});

Route::get('/lands', [LandController::class,"index"])->name('lands.index');
Route::get('/create', [LandController::class,"create"])->name('lands.create');
Route::post('/store', [LandController::class,"store"])->name('lands.store');
Route::get('/lands{id}/edit', [LandController::class,"edit"])->name('lands.edit');
Route::put('/lands{id}/update', [LandController::class,"update"])->name('lands.update');
Route::delete('/lands{id}/delete', [LandController::class,"destroy"])->name('lands.destroy');



