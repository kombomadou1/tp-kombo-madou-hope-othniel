@extends('layouts.main');
@section('content')
    <div class="col-md-10">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Ajoutez un pays</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
              <form method="POST" action="{{ route('lands.update', ['id' => $land->id]) }}">
                @csrf
                {{-- @method('POST') --}}
                <input type="hidden" name="_method" value="put" />

                <div class="card-body">
                  <div class="form-group row">
                    <label for="code_indicatif" class="col-sm-2 col-form-label">Code indicatif</label>
                    <div class="col-sm-10">
                      <input required type="text" class="form-control" name="code_indicatif" id="code_indicatif" placeholder="+xxx"  value="{{ $land->code_indicatif }}">
                    </div>
                </div>

                <div class="form-group row">
                  <label for="libellé" class="col-sm-2 col-form-label">Libellé</label>
                  <div class="col-sm-10">
                    <input required type="text" class="form-control" name="libelle" id="libellé" placeholder="Côte d'Ivoire" value="{{ $land->Libelle }}">
                  </div>
                </div>

                <div class="form-group row">
                      <label for="population" class="col-sm-2 col-form-label">Population</label>
                      <div class="col-sm-10">
                        <input required type="number" class="form-control" name="population" id="population" placeholder="Population" value="{{ $land->population }}">
                      </div>  
                </div>

                <div class="form-group row">
                      <label for="capital" class="col-sm-2 col-form-label">Capital</label>
                      <div class="col-sm-10">
                        <input required type="text" class="form-control" name="capital" id="capital" placeholder="Abidjan" value="{{ $land->capital }}">
                      </div>  
                  </div>

                <div class="form-group row">
                      <label for="superficie" class="col-sm-2 col-form-label">Superficie</label>
                      <div class="col-sm-10">
                        <input required type="number" class="form-control" name="superficie" id="superficie" placeholder="322462" value="{{ $land->Superficie }}">
                      </div>  
                </div>

                <div class="form-group row">
                      <label for="laique" class="col-sm-2 col-form-label">Laïque</label>
                     <div>
                      @if ($land->est_laique===1)
                        <input required type="radio" name="laique" value="1" id="oui" checked/> <label for="oui">oui</label>
                        <input required type="radio" name="laique" value="0"id="non" /> <label for="non">non</label>
                      @else
                        <input required type="radio" name="laique" value="1" id="oui" /> <label for="oui">oui</label>
                        <input required type="radio" name="laique" value="0"id="non" checked /> <label for="non">non</label>
                      @endif
                        
                    </div> 
                </div>

                <div class="form-group row">
                      <label for="langue" class="col-sm-2 col-form-label">Langue</label>
                      <div>
                        @if ($land->langue==="FR")
                          <input required type="radio" name="langue" value="FR" id="FR" checked/> <label for="FR">FR</label>
                          <input required type="radio" name="langue" value="EN"id="EN"  /> <label for="EN">EN</label>
                          <input required type="radio" name="langue" value="AR"id="AR"  /> <label for="AR">AR</label>
                          <input required type="radio" name="langue" value="ES" id="ES" /> <label for="ES">ES</label>
                        @elseif($land->langue==="EN")
                          <input required type="radio" name="langue" value="FR" id="FR" /> <label for="FR">FR</label>
                          <input required type="radio" name="langue" value="EN"id="EN"  checked/> <label for="EN">EN</label>
                          <input required type="radio" name="langue" value="AR"id="AR"  /> <label for="AR">AR</label>
                          <input required type="radio" name="langue" value="ES" id="ES" /> <label for="ES">ES</label>
                        @elseif($land->langue==="AR")
                          <input required type="radio" name="langue" value="FR" id="FR" /> <label for="FR">FR</label>
                          <input required type="radio" name="langue" value="EN"id="EN"  /> <label for="EN">EN</label>
                          <input required type="radio" name="langue" value="AR"id="AR"  checked/> <label for="AR">AR</label>
                          <input required type="radio" name="langue" value="ES" id="ES" /> <label for="ES">ES</label>
                        @elseif($land->langue==="ES")
                          <input required type="radio" name="langue" value="FR" id="FR" /> <label for="FR">FR</label>
                          <input required type="radio" name="langue" value="EN"id="EN"  /> <label for="EN">EN</label>
                          <input required type="radio" name="langue" value="AR"id="AR"  /> <label for="AR">AR</label>
                          <input required type="radio" name="langue" value="ES" id="ES" checked/> <label for="ES">ES</label>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                      <label for="Monnaie" class="col-sm-2 col-form-label">Monnaie</label>
                      <div>
                        @if ($land->monnaie==="XOF")
                          <input required type="radio" name="monnaie" value="XOF" id="XOF" checked/> <label for="XOF">XOF</label>
                          <input required type="radio" name="monnaie" value="EUR"id="EUR"  /> <label for="EUR">EUR</label>
                          <input required type="radio" name="monnaie" value="DOLLAR"id="DOLLAR"  /> <label for="DOLLAR">DOLLAR</label>
                        @elseif($land->monnaie==="EUR")
                          <input required type="radio" name="monnaie" value="XOF" id="XOF" /> <label for="XOF">XOF</label>
                          <input required type="radio" name="monnaie" value="EUR"id="EUR"  checked/> <label for="EUR">EUR</label>
                          <input required type="radio" name="monnaie" value="DOLLAR"id="DOLLAR"  /> <label for="DOLLAR">DOLLAR</label>
                        @elseif($land->monnaie==="DOLLAR")
                          <input required type="radio" name="monnaie" value="XOF" id="XOF" /> <label for="XOF">XOF</label>
                          <input required type="radio" name="monnaie" value="EUR"id="EUR"  checked/> <label for="EUR">EUR</label>
                          <input required type="radio" name="monnaie" value="DOLLAR"id="DOLLAR"  checked/> <label for="DOLLAR">DOLLAR</label>
                        @endif
                        
                    </div>
                </div>

                <div class="form-group row">
                    <label for="continent" class="col-sm-2 col-form-label">Contient</label>
                    <div>
                      @if ($land->Continent==="Afrique")
                          <input required type="radio" name="continent" value="Afrique" id="Afrique" checked/> <label for="Afrique">Afrique</label>
                          <input required type="radio" name="continent" value="Europe"id="Europe" /> <label for="Europe">Europe</label>
                          <input required type="radio" name="continent" value="Amerique"id="Amerique" value="{{ old('continent') }}"/> <label for="Amerique">Amérique</label>
                          <input required type="radio" name="continent" value="Asie" id="Asie" /> <label for="Asie">Asie</label>
                          <input required type="radio" name="continent" value="Océanie" id="Océanie" /> <label for="Océanie">Océanie</label>
                          <input required type="radio" name="continent" value="Antartique" id="Antartique" /> <label for="Antartique">Antartique</label>
                      @elseif($land->Continent==="Europe")
                          <input required type="radio" name="continent" value="Afrique" id="Afrique" /> <label for="Afrique">Afrique</label>
                          <input required type="radio" name="continent" value="Europe"id="Europe" checked /> <label for="Europe">Europe</label>
                          <input required type="radio" name="continent" value="Amerique"id="Amerique" value="{{ old('continent') }}"/> <label for="Amerique">Amérique</label>
                          <input required type="radio" name="continent" value="Asie" id="Asie" /> <label for="Asie">Asie</label>
                          <input required type="radio" name="continent" value="Océanie" id="Océanie" /> <label for="Océanie">Océanie</label>
                          <input required type="radio" name="continent" value="Antartique" id="Antartique" /> <label for="Antartique">Antartique</label>
                      @elseif($land->Continent==="Amerique")
                          <input required type="radio" name="continent" value="Afrique" id="Afrique" /> <label for="Afrique">Afrique</label>
                          <input required type="radio" name="continent" value="Europe"id="Europe"  /> <label for="Europe">Europe</label>
                          <input required type="radio" name="continent" value="Amerique"id="Amerique" checked/> <label for="Amerique">Amérique</label>
                          <input required type="radio" name="continent" value="Asie" id="Asie" /> <label for="Asie">Asie</label>
                          <input required type="radio" name="continent" value="Océanie" id="Océanie" /> <label for="Océanie">Océanie</label>
                          <input required type="radio" name="continent" value="Antartique" id="Antartique" /> <label for="Antartique">Antartique</label>
                      @elseif($land->Continent==="Asie")
                          <input required type="radio" name="continent" value="Afrique" id="Afrique" /> <label for="Afrique">Afrique</label>
                          <input required type="radio" name="continent" value="Europe"id="Europe"  /> <label for="Europe">Europe</label>
                          <input required type="radio" name="continent" value="Amerique"id="Amerique" value="{{ old('continent') }}"/> <label for="Amerique">Amérique</label>
                          <input required type="radio" name="continent" value="Asie" id="Asie" checked/> <label for="Asie">Asie</label>
                          <input required type="radio" name="continent" value="Océanie" id="Océanie" /> <label for="Océanie">Océanie</label>
                          <input required type="radio" name="continent" value="Antartique" id="Antartique" /> <label for="Antartique">Antartique</label>
                      @elseif($land->Continent==="Océanie")
                          <input required type="radio" name="continent" value="Afrique" id="Afrique" /> <label for="Afrique">Afrique</label>
                          <input required type="radio" name="continent" value="Europe"id="Europe"  /> <label for="Europe">Europe</label>
                          <input required type="radio" name="continent" value="Amerique"id="Amerique" value="{{ old('continent') }}"/> <label for="Amerique">Amérique</label>
                          <input required type="radio" name="continent" value="Asie" id="Asie" /> <label for="Asie">Asie</label>
                          <input required type="radio" name="continent" value="Océanie" id="Océanie" checked/> <label for="Océanie">Océanie</label>
                          <input required type="radio" name="continent" value="Antartique" id="Antartique" /> <label for="Antartique">Antartique</label>
                      @elseif($land->Continent==="Antartique")
                          <input required type="radio" name="continent" value="Afrique" id="Afrique" /> <label for="Afrique">Afrique</label>
                          <input required type="radio" name="continent" value="Europe"id="Europe" /> <label for="Europe">Europe</label>
                          <input required type="radio" name="continent" value="Amerique"id="Amerique" value="{{ old('continent') }}"/> <label for="Amerique">Amérique</label>
                          <input required type="radio" name="continent" value="Asie" id="Asie" /> <label for="Asie">Asie</label>
                          <input required type="radio" name="continent" value="Océanie" id="Océanie" /> <label for="Océanie">Océanie</label>
                          <input required type="radio" name="continent" value="Antartique" id="Antartique" checked/> <label for="Antartique">Antartique</label>

                      @endif
                        
                    </div>
                </div>

                <div class="form-group row">
                      <label for="description" class="col-sm-2 col-form-label">Description</label>
                      <div class="col-sm-10">
                        <textarea required name="description" id="description" cols="98" rows="10" placeholder="Décirvez le pays ici..." >{{ $land->description }}</textarea>
                      </div>  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Modifier</button>
                    {{-- <button type="submit" class="btn btn-default float-right">Annuler</button> --}}
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->
            <style>
              #Afrique, #Europe, #Amerique, #Asie, #Océanie, #Antartique,
              #FR, #EN, #AR, #ES,
              #XOF, #EUR, #DOLLAR,
              #oui, #non
              {
                margin-left: 2em;
              }
            </style>

          </div>
@endsection