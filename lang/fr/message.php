<?php

return [
    "customer" => [
        'all' => 'Liste des clients',
        "create" => "Le client a été crée avec success",
        "update" => "Modifié avec succes",
        "show" => "Detail d'un client",
        'delete' => 'client supprimé avec succes'
    ],
    "technicien" => [
        'all' => 'Liste des techniciens',
        "create" => "Le technicien a été crée avec success",
        "update" => "Modifié avec succes",
        "show" => "Detail d'un technicien",
        'delete' => 'Technicien supprimé avec succes'
    ],
    'formule' => [
        'all' => 'Liste des formules',
        'create' => 'Formule créee avec succes',
        'update' => 'Formule modifiée avec succes',
        'delete' => 'Formule supprimée avec succes'
    ],
    'role' => [
        'all' => 'Liste des roles',
        'create' => 'Role crée avec succes',
        'update' => 'Role modifié avec succes',
        'delete' => 'Role supprimé avec succes'
    ],
    'projet' => [
        'all' => 'Liste des projets',
        'create' => 'Projet crée avec succes',
        'update' => 'Projet modifié avec succes',
        'delete' => 'Projet supprimé avec succes',
    ],
    'phase' => [
        'projet' => "Liste des phase d'un projet",
        'all' => 'Liste des phases',
        'create' => 'Phase crée avec succes',
        'update' => 'Phase modifié avec succes',
        'delete' => 'Phase supprimé avec succes',
    ],
    'activite' => [
        'projet' => "Liste des activite d'un projet",
        'all' => 'Liste des activites',
        'create' => 'Activité crée avec succes',
        'update' => 'Activité modifié avec succes',
        'delete' => 'Activité supprimé avec succes',
        'show' => "Detail d'une activité",
    ],
];
